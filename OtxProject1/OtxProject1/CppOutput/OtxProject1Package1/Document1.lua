--Table Global
local tbl_Global = {}
--Init
local Init
--Signatures
--Procedures
tbl_Global.proc_main = {name = "main", document = "OtxProject1Package1:Document1", visibility = "PUBLIC"}
tbl_Global.proc_Procedure1 = {name = "Procedure1", document = "OtxProject1Package1:Document1", visibility = "PRIVATE"}
local llthreads2 = nil
local LoadLLThreads2 = function()
	if (llthreads2 == nil) then
		llthreads2 = require "llthreads2.ex"
	end
end

local isInitialized = false
function Init()
	if not(isInitialized) then
		isInitialized = true
		_OTX.Environment.AddImports("OtxProject1Package1:Document1", {})
	end
end
local StoreGlobalVariables = function()
	if (tbl_Global.isStoreGlobalVariables == true) then
		return
	end
	tbl_Global.isStoreGlobalVariables = true
	if (_OTX.Environment.StoreVariablesOfDocuments == nil) then
		return
	end
	_OTX.Environment.StoreVariablesOfDocuments("OtxProject1Package1.Document1", tbl_Global)
end
local ShareGlobalVariables = function()
	if (tbl_Global.isShareGlobalVariables == true) then
		return
	end
	tbl_Global.isShareGlobalVariables = true
	local b4emh00sg3s_tmp = _OTX.Environment.LoadGlobalVariables("OtxProject1Package1.Document1")
	for lew4sggaxui_key, zoqc5zzrcbp_value in pairs(b4emh00sg3s_tmp) do
		tbl_Global[lew4sggaxui_key] = zoqc5zzrcbp_value
	end
end

local bqobbdkgzzv = false
local function DisplayGlobalDeclarations()
	if not(bqobbdkgzzv) then
	end
	bqobbdkgzzv = true
end
tbl_Global.proc_main.validFor = function() return true end
tbl_Global.proc_main.procedure = function(tbl_Parameter)
	DisplayGlobalDeclarations()
	_OTX.Environment.RegisterProcedure("OtxProject1", "OtxProject1Package1", "Document1", "main")
	local id_41383247f14a491bb28b063693ffc95d_Status, id_41383247f14a491bb28b063693ffc95d_Return = pcall(function()
		--Table Parameter
		if (tbl_Parameter == nil) then
			tbl_Parameter = {}
		end
		--Table Local
		local tbl_Local = {}
		tbl_Local.var_String1 = _OTX.Variable.New("OtxProject1:OtxProject1Package1:Document1:main:String1", "", "String")
		tbl_Local.var_Nummer1 = _OTX.Variable.New("OtxProject1:OtxProject1Package1:Document1:main:Nummer1", "", "String")
		tbl_Local.var_String1:UpdateVariableTraceButSkipDefaultValue()
		tbl_Local.var_Nummer1:UpdateVariableTraceButSkipDefaultValue()
		--Action -  - Action_c3c2713c5ff24ab5aa7dc9193286769b
		if _OTX.Environment.IsNotTerminated() then
			local Action_c3c2713c5ff24ab5aa7dc9193286769b_Status, Action_c3c2713c5ff24ab5aa7dc9193286769b_Return = pcall(function()
				_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:main", "Activity Action_c3c2713c5ff24ab5aa7dc9193286769b will be executed")
				if true then
					_OTX.HMILib.ConfirmDialog(nil, "Start the new extracted procedure.", nil, nil)
				end
			end)
			if Action_c3c2713c5ff24ab5aa7dc9193286769b_Status then
				if Action_c3c2713c5ff24ab5aa7dc9193286769b_Return then
					if (Action_c3c2713c5ff24ab5aa7dc9193286769b_Return.Type == "return") then
						return
					end
				end
			else
				_OTX.Environment.Throw("Action_c3c2713c5ff24ab5aa7dc9193286769b", Action_c3c2713c5ff24ab5aa7dc9193286769b_Return)
			end
		end
		--Action - ProcedureCall1 - ProcedureCall_e206eebb5bda465ebb17f1d83c27620d
		if _OTX.Environment.IsNotTerminated() then
			local ProcedureCall_e206eebb5bda465ebb17f1d83c27620d_Status, ProcedureCall_e206eebb5bda465ebb17f1d83c27620d_Return = pcall(function()
				_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:main", "Activity ProcedureCall_e206eebb5bda465ebb17f1d83c27620d (ProcedureCall1) will be executed")
				if true then
					--Throws InvalidReferenceException if String1 is invalid (e.g. an uninitialized variable)
					local x41fyuaidb5 = tbl_Local.var_String1.Value
					--Throws InvalidReferenceException if Nummer1 is invalid (e.g. an uninitialized variable)
					local zk3eslxcqrs = tbl_Local.var_Nummer1.Value
					if tbl_Global.proc_Procedure1.validFor() then
						tbl_Global.proc_Procedure1.procedure({var_String1 = tbl_Local.var_String1, var_Nummer1 = tbl_Local.var_Nummer1})
					end
				end
			end)
			if ProcedureCall_e206eebb5bda465ebb17f1d83c27620d_Status then
				if ProcedureCall_e206eebb5bda465ebb17f1d83c27620d_Return then
					if (ProcedureCall_e206eebb5bda465ebb17f1d83c27620d_Return.Type == "return") then
						return
					end
				end
			else
				_OTX.Environment.Throw("ProcedureCall_e206eebb5bda465ebb17f1d83c27620d", ProcedureCall_e206eebb5bda465ebb17f1d83c27620d_Return)
			end
		end
	end)
	_OTX.Environment.UnregisterProcedure()
	if not(id_41383247f14a491bb28b063693ffc95d_Status) then
		error(id_41383247f14a491bb28b063693ffc95d_Return)
	end
end

tbl_Global.proc_Procedure1.validFor = function() return true end
tbl_Global.proc_Procedure1.procedure = function(tbl_Parameter)
	DisplayGlobalDeclarations()
	_OTX.Environment.RegisterProcedure("OtxProject1", "OtxProject1Package1", "Document1", "Procedure1")
	local Procedure_33006990cb054ac89d7428f6e9c4df21_Status, Procedure_33006990cb054ac89d7428f6e9c4df21_Return = pcall(function()
		--Table Parameter
		if (tbl_Parameter == nil) then
			tbl_Parameter = {}
		end
		if (tbl_Parameter.var_String1 == nil) then
			tbl_Parameter.var_String1 = _OTX.Variable.New("OtxProject1:OtxProject1Package1:Document1:Procedure1:String1", "", "String")
			tbl_Parameter.var_String1:UpdateVariableTraceButSkipDefaultValue()
		else
			if tbl_Parameter.var_String1:IsNotNull() then
				_OTX.Variable.UpdateVariableTraceButSkipValue("OtxProject1", "OtxProject1Package1.Document1", "Procedure1", "String1", tbl_Parameter.var_String1.Value, "String")
			end
		end
		if (tbl_Parameter.var_Nummer1 == nil) then
			tbl_Parameter.var_Nummer1 = _OTX.Variable.New("OtxProject1:OtxProject1Package1:Document1:Procedure1:Nummer1", "", "String")
			tbl_Parameter.var_Nummer1:UpdateVariableTraceButSkipDefaultValue()
		else
			if tbl_Parameter.var_Nummer1:IsNotNull() then
				_OTX.Variable.UpdateVariableTraceButSkipValue("OtxProject1", "OtxProject1Package1.Document1", "Procedure1", "Nummer1", tbl_Parameter.var_Nummer1.Value, "String")
			end
		end
		--Table Local
		local tbl_Local = {}
		--Action - Assignment1 - Action_7ff03bd655fa4c5f829ee368df02a18c
		if _OTX.Environment.IsNotTerminated() then
			local Action_7ff03bd655fa4c5f829ee368df02a18c_Status, Action_7ff03bd655fa4c5f829ee368df02a18c_Return = pcall(function()
				_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:Procedure1", "Activity Action_7ff03bd655fa4c5f829ee368df02a18c (Assignment1) will be executed")
				if true then
					tbl_Parameter.var_String1.Value = "Hello World"
				end
			end)
			if Action_7ff03bd655fa4c5f829ee368df02a18c_Status then
				if Action_7ff03bd655fa4c5f829ee368df02a18c_Return then
					if (Action_7ff03bd655fa4c5f829ee368df02a18c_Return.Type == "return") then
						return
					end
				end
			else
				_OTX.Environment.Throw("Action_7ff03bd655fa4c5f829ee368df02a18c", Action_7ff03bd655fa4c5f829ee368df02a18c_Return)
			end
		end
		--Action - DataSetNumberOrECUDataContaNumbe_Read1 - Action_bbf74dec94464752a82d651c9b7d002a
		if _OTX.Environment.IsNotTerminated() then
			local Action_bbf74dec94464752a82d651c9b7d002a_Status, Action_bbf74dec94464752a82d651c9b7d002a_Return = pcall(function()
				_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:Procedure1", "Activity Action_bbf74dec94464752a82d651c9b7d002a (DataSetNumberOrECUDataContaNumbe_Read1) will be executed")
				if true then
					if _OTX.Environment.IsNotTerminated() then
						local var_RequestList_403ebd9e8515477eb976a16f72ac82c4 = _OTX.List.New()
						local var_ResponseList_10501f37c22042a0bc1127da34bd6550 = _OTX.List.New()
						local var_expectedResponses_836e2c575a114cba987d8c262cd8d81d = _OTX.List.New({"PR_DataSetNumberOrECUDataContaNumbe_Read"})
						local var_ResponseParameter_31bdb70fdf2c4b4fb4e61e12e4f13939 = _OTX.Parameter.NewResponseParameter("PR_DataSetNumberOrECUDataContaNumbe_Read", "DataRecord.DataSetNumberOrECUDataContaNumbe_PartNumber", "DataSetNumberOrECUDataContaNumbe_PartNumber", tbl_Parameter.var_Nummer1)
						_OTX.List.Add(var_ResponseList_10501f37c22042a0bc1127da34bd6550, var_expectedResponses_836e2c575a114cba987d8c262cd8d81d)
						_OTX.List.Add(var_ResponseList_10501f37c22042a0bc1127da34bd6550, var_ResponseParameter_31bdb70fdf2c4b4fb4e61e12e4f13939)
						_OTX.DiagComLib.ExecuteDiagService(_OTX.DiagComLib.CreateDiagServiceByName(_OTX.DiagComLib.GetComChannel("LL_AirbaUDS", nil, true), "DataSetNumberOrECUDataContaNumbe_Read"), var_RequestList_403ebd9e8515477eb976a16f72ac82c4, var_ResponseList_10501f37c22042a0bc1127da34bd6550, nil, nil, false, false)
					end
				end
			end)
			if Action_bbf74dec94464752a82d651c9b7d002a_Status then
				if Action_bbf74dec94464752a82d651c9b7d002a_Return then
					if (Action_bbf74dec94464752a82d651c9b7d002a_Return.Type == "return") then
						return
					end
				end
			else
				_OTX.Environment.Throw("Action_bbf74dec94464752a82d651c9b7d002a", Action_bbf74dec94464752a82d651c9b7d002a_Return)
			end
		end
		--Branch - Branch1 - Branch_a45c1144c36b45bdbe0181d9b0680924
		if _OTX.Environment.IsNotTerminated() then
			local Branch_a45c1144c36b45bdbe0181d9b0680924_Status, Branch_a45c1144c36b45bdbe0181d9b0680924_Return = pcall(function()
				_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:Procedure1", "Activity Branch_a45c1144c36b45bdbe0181d9b0680924 (Branch1) will be executed")
				if (_OTX.StringUtilLib.SubString(tbl_Parameter.var_Nummer1.Value, 0, 1) == "9") then
					--Action - ConfirmDialog1 - Action_8a4927cf9abb48af8337afd169810c56
					if _OTX.Environment.IsNotTerminated() then
						local Action_8a4927cf9abb48af8337afd169810c56_Status, Action_8a4927cf9abb48af8337afd169810c56_Return = pcall(function()
							_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:Procedure1", "Activity Action_8a4927cf9abb48af8337afd169810c56 (ConfirmDialog1) will be executed")
							if true then
								_OTX.HMILib.ConfirmDialog(tbl_Parameter.var_String1.Value, tbl_Parameter.var_Nummer1.Value, nil, nil)
							end
						end)
						if Action_8a4927cf9abb48af8337afd169810c56_Status then
							if Action_8a4927cf9abb48af8337afd169810c56_Return then
								if (Action_8a4927cf9abb48af8337afd169810c56_Return.Type == "return") then
									return {Type="return"}
								elseif (Action_8a4927cf9abb48af8337afd169810c56_Return.Type == "break") then
									return {Type="break", Value=Action_8a4927cf9abb48af8337afd169810c56_Return.Value}
								elseif (Action_8a4927cf9abb48af8337afd169810c56_Return.Type == "continue") then
									return {Type="continue", Value=Action_8a4927cf9abb48af8337afd169810c56_Return.Value}
								end
							end
						else
							_OTX.Environment.Throw("Action_8a4927cf9abb48af8337afd169810c56", Action_8a4927cf9abb48af8337afd169810c56_Return)
						end
					end
				else
					--Action - ConfirmDialog2 - Action_fbf3d431f5934407b52396bd72b26521
					if _OTX.Environment.IsNotTerminated() then
						local Action_fbf3d431f5934407b52396bd72b26521_Status, Action_fbf3d431f5934407b52396bd72b26521_Return = pcall(function()
							_OTX.LoggingLib.WriteOtxIdIfNeeded("OtxProject1:OtxProject1Package1:Document1:Procedure1", "Activity Action_fbf3d431f5934407b52396bd72b26521 (ConfirmDialog2) will be executed")
							if true then
								_OTX.HMILib.ConfirmDialog(tbl_Parameter.var_String1.Value, "Wrong Number", _OTX.Enum.New("MessageType", 2), nil)
							end
						end)
						if Action_fbf3d431f5934407b52396bd72b26521_Status then
							if Action_fbf3d431f5934407b52396bd72b26521_Return then
								if (Action_fbf3d431f5934407b52396bd72b26521_Return.Type == "return") then
									return {Type="return"}
								elseif (Action_fbf3d431f5934407b52396bd72b26521_Return.Type == "break") then
									return {Type="break", Value=Action_fbf3d431f5934407b52396bd72b26521_Return.Value}
								elseif (Action_fbf3d431f5934407b52396bd72b26521_Return.Type == "continue") then
									return {Type="continue", Value=Action_fbf3d431f5934407b52396bd72b26521_Return.Value}
								end
							end
						else
							_OTX.Environment.Throw("Action_fbf3d431f5934407b52396bd72b26521", Action_fbf3d431f5934407b52396bd72b26521_Return)
						end
					end
				end
			end)
			if Branch_a45c1144c36b45bdbe0181d9b0680924_Status then
				if Branch_a45c1144c36b45bdbe0181d9b0680924_Return then
					if (Branch_a45c1144c36b45bdbe0181d9b0680924_Return.Type == "return") then
						return
					end
				end
			else
				_OTX.Environment.Throw("Branch_a45c1144c36b45bdbe0181d9b0680924", Branch_a45c1144c36b45bdbe0181d9b0680924_Return)
			end
		end
	end)
	_OTX.Environment.UnregisterProcedure()
	if not(Procedure_33006990cb054ac89d7428f6e9c4df21_Status) then
		error(Procedure_33006990cb054ac89d7428f6e9c4df21_Return)
	end
end


return {
	Init = Init, 
	StoreGlobalVariables = StoreGlobalVariables, 
	ShareGlobalVariables = ShareGlobalVariables, 
	proc_main = tbl_Global.proc_main, 
	proc_Procedure1 = tbl_Global.proc_Procedure1, 
	tbl_Global = tbl_Global
}
